# ICOM4075-Bonus-Points-Assignment

## Program Plan 
1. Bubble Sort
*  ~~Pseudo Code~~
* ~~Actual code~~

2. Verification Algorithm
* ~~Pseudo Code~~
* ~~Actual Code~~

3. Time Complexity
* **Pseudo Code**
* ~~Actual Code~~

4. **Implement into Jupyter Notebook**

5. References
