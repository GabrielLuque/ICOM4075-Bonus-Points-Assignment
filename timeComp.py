from sort     import bubbleSort
from ver      import verifySorted
from arrayGen import arrayGen

import datetime 
import time

def timeComplex(number):
	try:
		num = int(number)
	except ValueError:
		return "Error! The arguments is of a wrong data type! Please use a positive integer."


	array    = arrayGen(num)
	oldArray = list(array)

	if array:
		start = datetime.datetime.now()
		array = bubbleSort(array)
		end = datetime.datetime.now()
		upkeepTime = end - start

	if verifySorted(array) == True:
		#print("The array was sorted.")
		#print("The old array is:", oldArray)
		#print("The sorted array is:", array)
		#print("It was sorted in: ",upkeepTime.seconds,":",upkeepTime.microseconds)
		class returnValue:
			def __init__(self):
				self.oldArray = oldArray
				self.sorArray = array
				self.time     = upkeepTime
				
		return returnValue()

	if verifySorted(array) == False:
		print("Error! Array failed to be sorted.")

