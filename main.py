from timeComp import timeComplex

num = raw_input("How long would you like to generate the list? ")
result = timeComplex(num)
if type(result) != str:
	print "The array: \n",result.oldArray
	print "Has been sorted into: \n",result.sorArray
	print "It took",result.time.seconds,":",result.time.microseconds
else:
	print result
